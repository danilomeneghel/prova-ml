# Cadastro de DNA

Avaliação Técnica do Mercado Livre de um sistema de validação de DNA.

## Características

- CRUD
- Active Record
- API RestFul
- Login
- Search
- Routes
- Migrate
- Responsive
- Testes Unitários

## Requisitos

- Python >= 3.7.0
- Django >= 2.1.2
- PIP >= 18.0

## Tecnologias

- Python
- Django
- CSS and SCSS
- Jquery
- VueJS
- Bootstrap
- SQLite

## Instalação

```
$ git clone https://danilomeneghel@bitbucket.org/danilomeneghel/prova-ml.git

$ cd prova-ml
```

Download e instalação do Python 3: 

https://www.python.org/downloads/

PS: Defina a variável de ambiente apontando para o caminho da instalação do Python.


Download e instalação do PIP:

https://pip.pypa.io/en/stable/installing/

```
$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
$ python get-pip.py
$ python -m pip install -U pip
```

Em seguida, instale os requisitos do arquivo 'requirements.txt':

```
$ pip install -r requirements.txt
```

Depois que tudo estiver pronto, migre o banco de dados e execute o projeto:

```
$ python manage.py migrate
$ python manage.py runserver
```

Finalmente, abra o projeto em seu navegador:

http://localhost:8000/

## Criação de Novo Módulo

```
$ python manage.py startapp name_module
```

## Criação de Nova Migration (Tabela do Banco de Dados)

```
$ python manage.py makemigrations
```

## Rotas

```
$ python manage.py show_urls
```

## Testes

```
$ python manage.py test
```

## Demonstração

Você pode ver o sistema funcionando clicando no link abaixo:  
https://prova-ml.herokuapp.com/

- Login:
    - Usuário: admin
    - Senha: admin

## API REST

Endpoint GET DNAs:  
https://prova-ml.herokuapp.com/api/dna/

Endpoint POST DNA:  
https://prova-ml.herokuapp.com/api/simian/

Endpoint GET Stats:  
https://prova-ml.herokuapp.com/api/stats/

## Licença

Este projeto está licenciado sob <a href="LICENSE">The MIT License (MIT)</a>.

## Screenshots

![Screenshots](screenshots/screenshot01.png)    
![Screenshots](screenshots/screenshot02.png)    
![Screenshots](screenshots/screenshot03.png)    
![Screenshots](screenshots/screenshot04.png)    
![Screenshots](screenshots/screenshot05.png)    
![Screenshots](screenshots/screenshot06.png)    


Desenvolvido por  
Danilo Meneghel  
danilo.meneghel@gmail.com  
http://danilomeneghel.github.io/

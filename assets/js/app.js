new Vue({
  el: '#starting',
  delimiters: ['${','}'],
  data: {
    dna: [],
	dnas: [],
	stats: {},
	simian: 0,
	humano: 0,
    loading: true,
    successMessage: null,
	errorMessage: null,
    currentDna: { 'dna': [''] },
    search_term: '',
  },
  mounted: function() {
    this.getDnas();
	this.getStats();
  },
  methods: {
    //Função que valida se o dna é Símio ou não
	isSimian: function(currentDna) {
		var tipo = false;
		let regex = /^[ATCG]+$/;
		window['cont0']=1; window['cont1']=1; window['cont2']=1; window['cont3']=1; window['cont4']=1; window['cont5']=1;
		for(var value of currentDna.dna) {
			if(value.length != 6 || !regex.test(value)) {
				return undefined;
			} else {
				//Verifica dna de forma horizontal
				if(value.includes('AAAA') || value.includes('TTTT') || value.includes('CCCC') || value.includes('GGGG')) {
					tipo = true;
				} else {
					//Verifica dna de forma vertical
					for(x=0; x<value.length; x++) {
						window['cont'+x] = (value.substr(x,1) == window['c'+x]) ? window['cont'+x]+1 : 1;
						window['c'+x] = value.substr(x,1);						
						if(window['cont'+x] == 4) tipo = true;
					}
				}
			} 
		}
		return tipo;
	},
    getDnas: function() {
      let api_url = '/api/dna/';
      if(this.search_term!==''||this.search_term!==null) {
        api_url = `/api/dna/?search=${this.search_term}`
      }
      this.loading = true;
	  //Chama o endpoint dna e carrega os dados
      this.$http.get(api_url)
          .then((response) => {
            this.dnas = response.data;
            this.loading = false;
          })
          .catch((err) => {
            this.loading = false;
            console.log(err);
          })
    },
    getDna: function(id) {
      this.loading = true;
	  this.successMessage = null;
	  this.errorMessage = null;
	  if(id == null) {
		  this.currentDna.dna = [''];
		  this.currentDna.categoria = '';
		  this.loading = false;
	  } else {
		//Chama o endpoint dna_detail e carrega o dado do seu id
		this.$http.get(`/api/dna_detail/${id}/`)
		  .then((response) => {
			var arrDna = response.data.dna.split(", ");
			response.data.dna = arrDna;
			this.currentDna = response.data;
			this.loading = false;
		  })
		  .catch((err) => {
			this.loading = false;
			console.log(err);
		  })
	  }
    },
	addCodigo: function () {
      this.currentDna.dna.push('');
    },
    addDna: function() {
      this.loading = true;
	  //Pega os dados enviados, tira os espaços e coloca em uma nova variável
	  var currentDna = {'dna': this.currentDna.dna.map(s => s.trim())};
	  //Verifica se é um Simio
	  if(this.isSimian(currentDna) == undefined) {
		  this.successMessage = null;
		  this.errorMessage = "DNA inválido";
		  this.loading = false;
	  } else {
		  if(this.isSimian(currentDna))
			  currentDna.categoria = "Símio";
		  else
			  currentDna.categoria = "Humano";
		  //Transforma o array em uma string para salvar no banco
		  currentDna.dna = currentDna.dna.join(", ");
		  //Chama o endpoint simian e faz o post dos dados
		  this.$http.post('/api/simian/', currentDna)
			  .then((response) => {
				response.data.dna = response.data.dna.split(", ");
				this.currentDna = response.data;
				this.getDnas();
				this.errorMessage = null;
				this.successMessage = "Cadastrado com sucesso!";
				//$("#addDnaModal").modal('hide');
				this.loading = false;
			  })
			  .catch((err) => {
				this.getDnas();
				this.successMessage = null;
			    this.errorMessage = "DNA diferente de Símio ou falha ao salvar";
				this.loading = false;
				console.log(err);
			  })
	  } 
    },
    updateDna: function() {
      this.loading = true;
	  //Pega os dados enviados, tira os espaços e coloca em uma nova variável
	  var currentDna = {'dna': this.currentDna.dna.map(s => s.trim())};
	  //Verifica se é um Símio
	  if(this.isSimian(currentDna) == undefined) {
		  this.successMessage = null;
		  this.errorMessage = "DNA inválido";
		  this.loading = false;
	  } else {
		  if(this.isSimian(currentDna))
			  currentDna.categoria = "Símio";
		  else
			  currentDna.categoria = "Humano";
		  //Transforma o array em uma string para salvar no banco
		  currentDna.dna = currentDna.dna.join(", ");
		  //Chama o endpoint dna_detail e faz o update do dado do seu id
		  this.$http.put(`/api/dna_detail/${this.currentDna.id}/`, currentDna)
			  .then((response) => {
				response.data.dna = response.data.dna.split(", ");
				this.currentDna = response.data;
				this.getDnas();
				this.errorMessage = null;
				this.successMessage = "Editado com sucesso!";
				//$("#editDnaModal").modal('hide');
				this.loading = false;
			  })
			  .catch((err) => {
				this.getDnas();
				this.successMessage = null;
				this.errorMessage = "Falha ao salvar";
				this.loading = false;
				console.log(err);
			  })
	  }
    },
    deleteDna: function(id) {
      this.loading = true;
	  //Chama o endpoint dna_detail e faz o delete do dado do seu id
      this.$http.delete(`/api/dna_detail/${id}/`)
          .then((response) => {
            this.loading = false;
            this.getDnas();
          })
          .catch((err) => {
            this.loading = false;
            console.log(err);
          })
    },
	getStats: function() {
      this.loading = true;
	  //Chama o endpoint stats e mostra a estatística
	  this.$http.get('/api/stats/')
          .then((response) => {
			this.stats = response.data;
            this.loading = false;
          })
          .catch((err) => {
            this.loading = false;
            console.log(err);
          })
    }
  }
});

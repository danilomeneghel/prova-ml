from django.db import models

class Dna(models.Model):
    id = models.AutoField(primary_key=True)
    dna = models.CharField(max_length=250)
    categoria = models.CharField(max_length=250)
from rest_framework import serializers
from .models import Dna

class DnaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dna
        fields = '__all__'

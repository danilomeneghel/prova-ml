from django.urls import path
from .views import index, get_dnas, create_dna, dna_detail, statistic, stats

urlpatterns = [
    path('dna/', index, name='dna'),
    path('get_dnas/', get_dnas, name='get_dnas'),
    path('simian/', create_dna, name='simian'),
    path('dna_detail/<int:pk>/', dna_detail, name='dna_detail'),
    path('statistic/', statistic, name='statistic'),
    path('stats/', stats, name='stats'),
]

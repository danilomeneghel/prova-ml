from django.db import migrations, models

class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Dna',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('dna', models.CharField(max_length=250)),
                ('categoria', models.CharField(max_length=250)),
            ],
        ),
    ]

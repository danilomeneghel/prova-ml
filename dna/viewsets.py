from rest_framework import viewsets, filters
from .models import Dna
from .serializers import DnaSerializer

class DnaViewSet(viewsets.ModelViewSet):
    queryset = Dna.objects.all()
    serializer_class = DnaSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('dna', 'categoria')

from django.test import TestCase
from django.urls import reverse, resolve
from rest_framework import status
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from .models import Dna
from .serializers import DnaSerializer

class TestDnaView(TestCase):
    url = '/api/dna/'
    url_id = '/api/dna/{0}'

    def setUp(self):
        #self.dna = Dna.objects.create(dna='AAAATT', categoria='Símio')
        self.dna = Dna()
        self.dna.dna = 'AAAATT'
        self.dna.categoria = 'Símio'
        self.dna.save()
        self.client = APIClient()
        self.data = {'dna': 'CCTAGG', 'categoria': 'Humano'}

    def test_dnas_status_code(self):
        request = self.client.get(self.url)
        self.assertEqual(request.status_code, status.HTTP_200_OK)

    def test_dna_post_status_code(self):
        request = self.client.post(self.url, self.data)
        self.assertEqual(request.status_code, status.HTTP_201_CREATED)

    def test_dna_get_status_code(self):
        request = self.client.get(self.url_id.format(self.dna.pk))
        self.assertEquals(request.status_code, status.HTTP_301_MOVED_PERMANENTLY)

    def test_dna_put_status_code(self):
        request = self.client.put(self.url_id.format(self.dna.pk), self.data)
        self.assertEqual(request.status_code, status.HTTP_301_MOVED_PERMANENTLY)

    def test_dna_delete_status_code(self):
        request = self.client.delete(self.url_id.format(self.dna.pk))
        self.assertEqual(request.status_code, status.HTTP_301_MOVED_PERMANENTLY)

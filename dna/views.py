from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from django.shortcuts import render
from .models import Dna
from .serializers import DnaSerializer
from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    return render(request, 'index.html')

@api_view(['GET'])
def get_dnas(request):
    dnas = Dna.objects.all()
    serializer = DnaSerializer(dnas, many=True, context={'request': request})
    return Response(serializer.data)

@api_view(['POST'])
def create_dna(request):
	serializer = DnaSerializer(data=request.data)
	if serializer.is_valid():
		serializer.save()
		if request.data['categoria'] == 'Símio':
			return Response(serializer.data, status=status.HTTP_200_OK)
		else:
			return Response(serializer.data, status=status.HTTP_403_FORBIDDEN)
	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def dna_detail(request, pk):
	try:
		dna = Dna.objects.get(pk=pk)
	except Dna.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = DnaSerializer(dna)
		return Response(serializer.data)

	elif request.method == 'PUT':
		serializer = DnaSerializer(dna, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_200_OK)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		dna.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def stats(request):
    count_mutant_dna = 0;
    count_human_dna = 0;

    dnas = Dna.objects.all()
    for dna in dnas:
        if dna.categoria == 'Símio':
            count_mutant_dna = count_mutant_dna + 1;
        else:
            count_human_dna = count_human_dna + 1;
    ratio = count_mutant_dna / count_human_dna;

    return Response({'count_mutant_dna': count_mutant_dna, 'count_human_dna': count_human_dna, 'ratio': ratio})

@login_required
def statistic(request):
    return render(request, 'statistic.html')

from django.contrib import admin
from django.urls import path, include
from .routers import router
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api/', include('dna.urls')),
    path('', include(('dna.urls', 'dna'), namespace='dna')),
    path('', include(('users.urls', 'users'), namespace='users')),
]

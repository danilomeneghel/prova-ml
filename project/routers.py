from rest_framework import routers
from dna.viewsets import DnaViewSet

router = routers.DefaultRouter()

router.register(r'dna', DnaViewSet)